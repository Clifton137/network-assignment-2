import java.util.ArrayList;

public class Student {
     boolean mask;
     ArrayList<Integer> schedule;
     int assignedDesk;

     public Student(boolean mask, ArrayList<Integer> schedule, int assignedDesk)
     {
         this.mask = mask;
         this.schedule = new ArrayList<Integer>(schedule);
         this.assignedDesk = assignedDesk;
     }
}
